package com.example.haukap.chess;

import java.util.*;
/**
 *  Created by haukap on 7/17/17.
 */

public class ChessEngine {

    private static final int MAX_ROW  = 8;
    private static final char MAX_COL  = 'h';
    private static final char MIN_COL = 'a';
    private static final int MIN_ROW = 1;
    private Map<Piece, Map<String, Vector<Integer> > > m_legalMoves = new HashMap<>();
    private Map< Piece, Map<String, Vector<Integer> > > m_nextMove =  new HashMap<>();//pieceId, col, rows
    private Map<String, Map<Integer, Piece> >m_chessBoard = new HashMap<>(); //col, row , PieceId
    private Map<Integer, Map<String, Vector< Piece> > > m_castleMap = new HashMap<>();
    private Vector< Vector<String> > m_loadGame = new Vector<>();
    private Vector<String> m_chessGame = new Vector<>();
    Map<Integer, Map<Integer, Piece> > m_pieceLocations = new HashMap<>();
    private int m_player;
    private boolean m_inCheckP1;
    private boolean m_inCheckP2;
    private boolean m_castle;
    private int m_fromRow;
    private int m_toRow;
    private char m_fromCol;
    private char m_toCol;
    char m_buttonFromCol;
    int m_buttonFromRow;
    char m_king;
    char m_queen;
    char m_bishop;
    char m_knight;
    char m_pawn;
    char m_rook;
    private AndroidRenderer m_renderer;
    private Piece m_piece;
    private AndroidIO m_io;
    private final static int NONE = 0;
    private final static int PLAYER1 = 1;
    private final static int PLAYER2 = 2;
    private final static char EMPTY = 'O';
    private final static int KING = 'K';
    private final static int QUEEN = 'Q';
    private final static int KNIGHT = 'N';
    private final static int ROOK = 'R';
    private final static int BISHOP = 'B';
    private final static int PAWN = 'P';

    private final static char COL_A = 'a';
    private final static char COL_B = 'b';
    private final static char COL_C = 'c';
    private final static char COL_D = 'd';
    private final static char COL_E = 'e';
    private final static char COL_F = 'f';
    private final static char COL_G = 'g';
    private final static char COL_H = 'h';


     ChessEngine(){

        m_player = 1;
        m_inCheckP1 = false;
        m_inCheckP2 = false;
        m_castle = true;
    }

    private void deleteLegalMoves(Map<Piece, Map<String, Vector<Integer>>> legal)
    {
        legal.clear();
    }


    private void deleteNextMoves(Map<Piece, Map<String, Vector<Integer>>> nextMoves)
    {
        nextMoves.clear();
    }

    private boolean isCastlePathValid(Piece king, Piece rook)
    {
        char rookCol = rook.m_col;
        int row = rook.m_row;
        char kingCol = king.m_col;
        Map<Integer, Piece > innerMap;

        //queen side castle
        if(kingCol < rookCol)
        {
            for(rookCol -= 1; rookCol > kingCol; --rookCol)
            {
                innerMap = m_chessBoard.get(rookCol);
                Piece p = innerMap.get(row);
                //if(m_chessBoard.get(rookCol)][row].m_pieceType != Empty)
                if(p.m_pieceType != EMPTY)
                    return false;
            }
        }

        //king side castle
        if(kingCol > rookCol)
        {
            for(rookCol += 1; rookCol < kingCol; ++rookCol)
            {
                innerMap = m_chessBoard.get(rookCol);
                Piece p = innerMap.get(row);
                //if(m_chessBoard[rookCol][row].m_pieceType != Empty)
                if(p.m_pieceType != EMPTY)
                    return false;
            }
        }

        return true;
    }


    private void setCastleMoves()
    {
        Piece king;
        Piece rook;
        //java.util.Iterator it = castleMap.keySet().iterator();
        //java.util.Iterator it2 = Map<String, ArrayList< com.example.haukap.chess.Piece> >::iterator it2;
        Map<String, Vector<Piece> > inner;
        //assert(m_castleMap.size() != 0);

        //player1

        //king not in check
        if(!m_inCheckP1)
        {
            boolean found = m_castleMap.containsKey(PLAYER1);
            //it = m_castleMap.find(Player1);
            Vector< Piece > vec;
            // if(it != m_castleMap.end() )
            if(found)
            {
                inner = m_castleMap.get(PLAYER1);
                boolean isKing = inner.containsKey(KING);
                // if(it2 != inner.end() )
                if(isKing)
                {
                    vec = inner.get(KING);
                    king = vec.get(0);


                    //king in initial position
                    if(king.m_initPosition)
                    {
                        boolean isFound = inner.containsKey(ROOK);
                        Vector< Piece > vec2;
                        //rook found
                        if(isFound)
                        {
                            vec2 = inner.get(ROOK);
                            for(int i = 0; i < vec2.size(); ++i)
                            {
                                rook = vec2.get(i);
                                //rook hasn't moved
                                if(rook.m_initPosition)
                                {
                                    if(isCastlePathValid(king, rook) )
                                    {
                                        //add path to legalMoves
                                        Map<String, Vector<Integer> > location;
                                        location = m_legalMoves.get(king);
                                        Vector<Integer> rows;
                                        rows = location.get(rook.m_col);
                                        rows.add(rook.m_row);
                                        String column =Character.toString(rook.m_col);
                                        location.put(column, rows);
                                        m_legalMoves.put(king, location);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //player2
        //king not in check
        if(!m_inCheckP2)
        {
            //king and rook have not moved

        }


        //no pieces between king and rook

        //king moves through sq that is attacked by opponent

        //king in check after castle
    }

    public void addPieceLocation(int x, int y, char col, int row)
    {
        //add piece location, x, y to location map
    }

    private void addPieceToCastleMap(Piece p)
    {
        int player = p.m_player;
        char  type = p.m_pieceType;
        String s = Character.toString(type);
        Vector<Piece> vec;
        //add rook to castling map
        Map<String, Vector< Piece> > innerMap;

        boolean found = m_castleMap.containsKey(player);

        //player in castleMap
        if(found)
        {
            innerMap = m_castleMap.get(player);
            boolean inMap = innerMap.containsKey(s);

            //pieceType not in map
            if(!inMap)
            {
                vec = new Vector<>();
                vec.add(p);
                innerMap.put(s, vec);
                m_castleMap.put(player, innerMap);
            }
            else
            {
                //pieceType in map
                innerMap = m_castleMap.get(player);
                vec = innerMap.get(s);
                vec.add(p);
                
                innerMap.put(s, vec);
                m_castleMap.put(player, innerMap);
            }
        }
        else
        {
            //piece not in castle map
            vec = new Vector<>();
            vec.add(p);
            innerMap = new HashMap< >();
            innerMap.put(s, vec);
            m_castleMap.put(player, innerMap);
        }

    }

    //finds all legal moves for both players for handling in check
    void findAllNextMoves(Piece p)
    {
        //switch current player to get moves for next player
        switchPlayer();
        findAllLegalMoves();
        //reset current player
        switchPlayer();
    }

    //finds all legal moves for current player
    private void findAllLegalMoves()
    {
        char type = 'z';
        Piece p;
        Map<Integer, Piece> innerMap;
        String s;

        //access each col in board
        for(char col = MIN_COL; col <= MAX_COL; ++col)
        {
            //for current players pieces, add legal moves to map
            //iter over chessboard to get each piece
            for(int row = MIN_ROW; row<= MAX_ROW; ++row)
            {
                s = Character.toString(col);
                innerMap = m_chessBoard.get(s);
                p = innerMap.get(row);

                if(p.m_player == m_player)
                {
                    type = p.m_pieceType;
                    //for each piece find legal moves
                    switch(type)
                    {
                        case PAWN :{
                            findPawnMoves(p);
                            break;
                        }

                        case QUEEN :{
                            findVerticalMoves(p);
                            findHorizontalMoves(p);
                            findDiagonalMoves(p);
                            break;
                        }

                        case KING :{
                            addPieceToCastleMap(p);
                            findKingMoves(p);
                            break;
                        }

                        case KNIGHT : {
                            findKnightMoves(p);
                            break;
                        }

                        case ROOK :{
                            addPieceToCastleMap(p);
                            findVerticalMoves(p);
                            findHorizontalMoves(p);
                            break;
                        }

                        case BISHOP :{
                            findDiagonalMoves(p);
                            break;
                        }

                        default:
                            //assert( 0 && "shit broken");
                            break;

                    }
                }
            }
        }
        setCastleMoves();
    }


    //
    private void findKnightMoves(Piece p)
    {

        char col = p.getCol();
        int row = p.getRow();
        int rowPlusOne = row + 1;
        int rowPlusTwo = row + 2;
        int rowMinusOne = row - 1;
        int rowMinusTwo = row - 2;
        char colPlusOne = col;
        colPlusOne += 1;
        char colPlusTwo = col;
        colPlusTwo +=  2;
        char colMinusOne = col;
        colMinusOne -= 1;
        char colMinusTwo = col;
        colMinusTwo -= 2;
        Map<Integer, Piece>  innerMap;

        //+ horizontal 2, + v:1
        if(colPlusTwo <= MAX_COL && rowPlusOne <= MAX_ROW)
        {
            innerMap = m_chessBoard.get(colPlusTwo);
            Piece next = innerMap.get(rowPlusOne);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colPlusTwo, rowPlusOne);
                addMoveToNextMove(p, colPlusTwo, rowPlusOne);
                //king in check
                if(next.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
            }
        }

        // + h:2, - v:1
        if(colPlusTwo <= MAX_COL && rowMinusOne >= MIN_ROW)
        {
            innerMap = m_chessBoard.get(colPlusTwo);
            Piece next = innerMap.get(rowMinusOne);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colPlusTwo, rowMinusOne);
                addMoveToNextMove(p, colPlusTwo, rowMinusOne);
            }
        }

        //- h:2, +v:1
        if(colMinusTwo >= MIN_COL && rowPlusOne <= MAX_ROW)
        {
            innerMap = m_chessBoard.get(colMinusTwo);
            Piece next = innerMap.get(rowPlusOne);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colMinusTwo, rowPlusOne);
                addMoveToNextMove(p, colMinusTwo, rowPlusOne);
            }
        }

        //-h:2, -v:1
        if(colMinusTwo >= MIN_COL && rowMinusOne >= MIN_ROW)
        {
            innerMap = m_chessBoard.get(colMinusTwo);
            Piece next = innerMap.get(rowMinusOne);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colMinusTwo, rowMinusOne);
                addMoveToNextMove(p, colMinusTwo, rowMinusOne);
            }
        }

        //+h:1, +v:2
        if(colPlusOne <= MAX_COL && rowPlusTwo <= MAX_ROW)
        {
            innerMap = m_chessBoard.get(colPlusOne);
            Piece next = innerMap.get(rowPlusTwo);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colPlusOne, rowPlusTwo);
                addMoveToNextMove(p, colPlusOne, rowPlusTwo);
            }
        }

        //-h:1, +v:2
        if(colMinusOne >= MIN_COL && rowPlusTwo <= MAX_ROW)
        {
            innerMap = m_chessBoard.get(colMinusOne);
            Piece next = innerMap.get(rowPlusTwo);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colMinusOne, rowPlusTwo);
                addMoveToNextMove(p, colMinusOne, rowPlusTwo);
            }
        }

        //+h:1, -v:2
        if(colPlusOne <= MAX_COL && rowMinusTwo >= MIN_ROW)
        {
            innerMap = m_chessBoard.get(colPlusOne);
            Piece next = innerMap.get(rowMinusTwo);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colPlusOne, rowMinusTwo);
                addMoveToNextMove(p, colPlusOne, rowMinusTwo);
            }
        }
        //-h:1, -v:2
        if(colMinusOne >= MIN_COL && rowMinusTwo >= MIN_ROW)
        {
            innerMap = m_chessBoard.get(colMinusOne);
            Piece next = innerMap.get(rowMinusTwo);
            if(next.m_player != m_player)
            {
                addMoveToLegalMoves(p, colMinusOne, rowMinusTwo);
                addMoveToNextMove(p, colMinusOne, rowMinusTwo);
            }
        }
    }

    //finds legal king moves
    private void findKingMoves(Piece p)
    {

        char col = p.getCol();
        int row = p.getRow();
        char type = p.m_pieceType;
        int player = p.m_player;
        char currCol = col;
        int currRow = row;
        Map<Integer, Piece>  innerMap = new HashMap<>();

        //find +col, +row direction
        currCol += 1;
        currRow += 1;

        //add king to castling map
        Map<String, Vector< Piece> > inner = new HashMap< >();
        Vector< Piece> vec = new Vector<>();
        vec.add(p);
        String s = Character.toString(type);
        inner.put(s, vec);
        m_castleMap.put(player, inner);

        if(currCol <= MAX_COL && currRow <= MAX_ROW)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            //empty sq check, if yes then add move to legal moves
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, currCol, currRow);
                addMoveToNextMove(p, currCol, currRow);
            }
        }

        //check -col, -row
        currCol = col;
        currCol -= 1;
        currRow = row;
        currRow -= 1;
        if(currCol >= MIN_COL && currRow >= MIN_ROW)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            //empty sq check, if yes then add move
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, currCol, currRow);
                addMoveToNextMove(p, currCol, currRow);
            }
        }

        //check -col, +row
        currCol = col;
        currCol -= 1;
        currRow = row;
        currRow += 1;
        //check -col, +row
        if(currCol >= MIN_COL && currRow <= MAX_ROW)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            //empty sq check, if yes then add move
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, currCol, currRow);
                addMoveToNextMove(p, currCol, currRow);
            }
        }

        currCol = col;
        currCol += 1;
        currRow = row;
        currRow -= 1;

        //check +col, -row
        if(currCol <= MAX_COL && currRow >= MIN_ROW)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            //empty sq check, if yes then add move
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, currCol, currRow);
                addMoveToNextMove(p, currCol, currRow);
            }
        }

        currCol = col;
        currCol += 1;
        //check horiz + direction
        if(currCol <= MAX_COL)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, currCol, row);
                addMoveToNextMove(p, currCol, row);
            }
        }

        currCol = col;
        currCol -= 1;
        //check horiz - direction
        if(currCol >= MIN_COL)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, currCol, row);
                addMoveToNextMove(p, currCol, row);
            }
        }

        currRow = row;
        currRow += 1;
        //check vertical + direction
        if(currRow <= MAX_ROW)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, col, currRow);
                addMoveToNextMove(p, col, currRow);
            }
        }

        currRow = row;
        currRow -= 1;
        //check vertical - direction
        if(currRow >= MIN_ROW)
        {
            innerMap = m_chessBoard.get(currCol);
            Piece next = innerMap.get(currRow);
            if(next.m_player == NONE)
            {
                addMoveToLegalMoves(p, col, currRow);
                addMoveToNextMove(p, col, currRow);
            }
        }
    }


    //validate any number of vertical moves
    private void findVerticalMoves(Piece p)
    {

        char col = p.getCol();
        int row = p.getRow();
        Piece pNext;//piece for next move
        Map<String, Vector<Integer> > innerMap;//chessboard, col
        Vector<Integer> vec;
        Map<Integer,  Piece > inner;//inner chessboard, row, piece

        ++row;
        //check pieces current row through 8
        //for given col and row, check row - MAX row for valid move
        while(row <= MAX_ROW)
        {
            //get possible moves next piece from chessboard square
            inner = m_chessBoard.get(col);
            pNext = inner.get(row);

            //opponent piece square, find square as valid
            if(pNext.m_player != NONE && pNext.m_player != m_player)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);

                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }
            //current players piece, do not add sq as valid
            if(pNext.m_player == m_player)
            {
                break;
            }
            //empty square
            if(pNext.m_player == NONE)
            {
                //add move in legal moves map
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            ++row;
        }

        row = p.getRow();
        --row;
        //check pieces current row through 1
        //for given col and row, check row - MIN row for valid move
        while(row >= MIN_ROW)
        {
            inner = m_chessBoard.get(col);
            pNext = inner.get(row);

            //opponent piece square
            if(pNext.m_player != m_player && pNext.m_player != NONE)
            {
                //add move in legal moves map
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }
            //current players piece
            if(pNext.m_player == m_player)
            {
                break;
            }
            //empty square
            if(pNext.m_player == NONE)
            {
                //add move in legal moves map
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            --row;
        }
    }

    //validate "any" number of horizontal moves
    private void findHorizontalMoves(Piece p)
    {

        char col = p.getCol();
        int row = p.getRow();
        Piece pNext;//piece in square for next move
        Map<String, Vector<Integer> > innerMap;//chessboard, col
        Vector<Integer> vec;
        Map<Integer,  Piece > inner;//inner chessboard, row, piece

        ++col;
        //check pieces current col through h
        //for given col and row, check col - MAX col for valid move
        while(col <= MAX_COL)
        {
            //piece on next move square
            inner = m_chessBoard.get(col);
            pNext = inner.get(row);

            //opponent piece square, add square as valid
            if(pNext.m_player != m_player && pNext.m_player != NONE)
            {
                //set move in legal moves map
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }
            //current players piece, do not add sq as valid
            if(pNext.m_player == m_player)
            {
                break;
            }
            //empty square
            if(pNext.m_player == NONE)
            {
                //find move in legal moves map
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            ++col;
        }

        col = p.getCol();
        --col;
        //check pieces current col through col a
        //for given col and row, check col - MIN col for valid move
        while(col >= MIN_COL)
        {
            //piece on next move square
            inner = m_chessBoard.get(col);
            pNext = inner.get(row);
            //opponent piece square
            if(pNext.m_player != m_player && pNext.m_player != NONE)
            {
                //add move in legal moves map
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }
            //current players piece
            if(pNext.m_player == m_player)
            {
                break;
            }
            //empty square
            if(pNext.m_player == NONE)
            {
                //add move in legal moves map
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            --col;
        }
    }

    //validate any number of diagonal moves
    private void findDiagonalMoves(Piece p)
    {

        char col = p.getCol();
        int row = p.getRow();
        Piece pNext; //next squares piece
        Map< Piece, Map<String, Vector<Integer> > > outerMap;
        Map<String, Vector<Integer> > innerMap;
        Map<String, Vector<Integer> > it;
        Map<Integer, Piece> innerChessBoard;

        ++row;
        ++col;

        while(col <= MAX_COL && row <= MAX_ROW)
        {
            //check squares in +row, +col diagonal direction
            innerChessBoard = m_chessBoard.get(col);
            pNext = innerChessBoard.get(row);

            //current players piece, break
            if( pNext.m_player == m_player)
            {
                break;
            }

            //opponent piece, add move to legal moves and break
            if( pNext.m_player != m_player && pNext.m_player != NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }

            //empty square, add to legal moves
            if(pNext.m_player == NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            ++col;
            ++row;
        }


        col = p.getCol();
        row = p.getRow();
        //check squares in -row, -col diagonal direction
        --row;
        --col;

        while(col >= MIN_COL && row >= MIN_ROW)
        {
            innerChessBoard = m_chessBoard.get(col);
            pNext = innerChessBoard.get(row);

            //current players piece, break
            if( pNext.m_player == m_player)
            {
                break;
            }

            //opponent piece, add move to legal moves and break
            if( pNext.m_player != m_player && pNext.m_player != NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }

            //empty square, add to legal moves
            if(pNext.m_player == NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            --col;
            --row;
        }

        col = p.getCol();
        row = p.getRow();
        --row;
        ++col;

        //check squares in -row, +col diagonal direction
        while(row >= MIN_ROW && col <= MAX_COL)
        {
            innerChessBoard = m_chessBoard.get(col);
            pNext = innerChessBoard.get(row);

            //current players piece, break
            if( pNext.m_player == m_player)
            {
                break;
            }

            //opponent piece, add move to legal moves and break
            if( pNext.m_player != m_player && pNext.m_player != NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }

            //empty square, add to legal moves
            if(pNext.m_player == NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            --row;
            ++col;
        }

        col = p.getCol();
        row = p.getRow();
        --col;
        ++row;
        //check squares in +row, -col diagonal direction
        while(row <= MAX_ROW && col >= MIN_COL)
        {
            innerChessBoard = m_chessBoard.get(col);
            pNext = innerChessBoard.get(row);

            //current players piece, break
            if( pNext.m_player == m_player)
            {
                break;
            }

            //opponent piece, add move to legal moves and break
            if( pNext.m_player != m_player && pNext.m_player != NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
                //king in check
                if(pNext.m_pieceType == KING)
                {
                    //set king as inCheck
                    if(m_player == PLAYER1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
                break;
            }

            //empty square, add to legal moves
            if(pNext.m_player == NONE)
            {
                addMoveToLegalMoves(p, col, row);
                addMoveToNextMove(p, col, row);
            }
            --col;
            ++row;
        }
    }

    //add move to next move map to use for handling king in check
    private void addMoveToNextMove(Piece p, char col, int row)
    {
        Map<String, Vector<Integer> > innerMap;
        boolean found = m_nextMove.containsKey(p);
        Vector<Integer> vec;

        //piece in legal moves map
        if(found)
        {
            innerMap = m_nextMove.get(p);
            boolean isFound = innerMap.containsKey(col);

            //col not in map
            if(!isFound )
            {
                vec = new Vector<>();
                vec.add(row);
                String s = Character.toString(col);
                innerMap.put(s, vec);
                m_nextMove.put(p, innerMap);
            }
            else
            {
                //col in map
                vec = innerMap.get(col);
                vec.add(row);
                String s = Character.toString(col);
                innerMap.put(s, vec);
                m_nextMove.put(p, innerMap);
            }
        }
        else
        {
            //piece not in legal map
            vec = new Vector<>();
            vec.add(row);
            String s = Character.toString(col);
            innerMap = new HashMap<>();
            innerMap.put(s, vec);
            m_nextMove.put(p, innerMap);
        }
    }

    //add legal move
    private void addMoveToLegalMoves(Piece p, char col, int row)
    {
        Map<String, Vector<Integer> > innerMap;
        Vector<Integer> vec;
        String s = Character.toString(col);

        boolean found = m_legalMoves.containsKey(p);

        //piece in legal moves map
        if(found )
        {
            innerMap = m_legalMoves.get(p);
            boolean isFound = innerMap.containsKey(col);

            //col not in map
            if(!isFound )
            {
                vec = new Vector<>();
                vec.add(row);
                innerMap.put(s, vec);
                m_legalMoves.put(p, innerMap);
            }
            else
            {
                //col in map
                vec = innerMap.get(col);
                vec.add(row);
                innerMap.put(s, vec);
                m_legalMoves.put(p, innerMap);
            }
        }
        else
        {
            //piece not in legal map
            vec = new Vector<>();
            vec.add(row);
            innerMap = new HashMap<>();
            innerMap.put(s, vec);
            m_legalMoves.put(p, innerMap);
        }
    }

    //verify legal move by checking legalMoves map
    private boolean verifyLegalMove()
    {
        Map<Integer, Piece> innerMap = m_chessBoard.get(m_fromCol);
        Piece p = innerMap.get(m_fromRow);

        //player owns piece
        if(p.m_player == m_player)
        {
            //valid move found
            Map<String, Vector<Integer> > loc = m_legalMoves.get(p);
            boolean found = loc.containsKey(m_toCol);

            //column in legal moves
            if(found)
            {
                Vector<Integer> vec = loc.get(m_toCol);
                //look for move in legal list
                for(int i = 0; i < vec.size(); ++i)
                {
                    //row found, move is legal
                    if(m_toRow == vec.get(i) )
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //load game input textfile or run game input
    void processGameInput(String action)
    {
        String run = "r";
        String load = "l";

        //run game
        if(action.equals(run) )
        {
            initChessBoard();
            runTerminalGame();
        }
        if(action.equals(load))
        {
            initEmptyChessBoard();
            loadGameBoard();
        }
    }

    private void setPiece(Piece p, char col, int row, char type, int player, boolean position)
    {
        p.m_col = col;
        p.m_row = row;
        p.m_pieceType = type;
        p.m_player = player;
        p.m_initPosition = position;
    }

    private void loadGameBoard()
    {
        Piece p;
        char pieceType = EMPTY;
        char col = 'z';
        int row = 0;
        int player = NONE;
        int positionVal = 1;
        boolean position  = false;
        int pos = 0;
        int index0 = 0;//col
        int index1 = 1;//row
        int index2 = 2;//piece type
        int index3 = 3;//player
        int index4 = 4;//orig position of piece
        Vector<String>  vec;
        Map<Integer, Piece> innerMap;
        String temp;

        initEmptyChessBoard();

        //set com.example.haukap.chess.Piece data
        for(int i = 0; i < m_loadGame.size(); ++i)
        {
            vec = m_loadGame.get(i);
            temp = vec.get(index0);
            col = temp.charAt(index0);//first char in string
            row = Integer.parseInt(vec.get(index1));

            temp = vec.get(index2);
            pieceType = temp.charAt(index0);


            player = Integer.parseInt(vec.get(index3) );
            pos = Integer.parseInt(vec.get(index4));
            if(pos == positionVal) {
                position = true;
            }
            if(pos != positionVal)
            {
                position = false;
            }
            innerMap = m_chessBoard.get(col);
            p = innerMap.get(row);
            setPiece(p, col, row, pieceType, player, position);
        }
        gameLoop();
    }

    //run game from text file
    private void runTerminalGame()
    {
        initChessBoard();
        String temp;
        int zero = 0;
        int one = 1;

        //iter over vector and for each pair set to/from locations
        for(int i = 0, j = 1; j < m_chessGame.size(); i +=2, j += 2)
        {
            m_renderer.printChessBoard(m_chessBoard);
            findAllLegalMoves();
            //set chessEngine to/from locations for move
            temp = m_chessGame.get(i);
            m_fromCol = temp.charAt(zero);
            m_fromRow = temp.charAt(one);

            temp = m_chessGame.get(j);
            m_toCol = temp.charAt(zero);
            m_toRow = temp.charAt(one);

            updateChessBoard();
            switchPlayer();
            deleteLegalMoves(m_legalMoves);
            deleteNextMoves(m_nextMove);
        }
        gameLoop();
    }


    void gameLoop()
    {
        //runTerminalGame();
       // m_renderer.printChessBoard(m_chessBoard);
        findAllLegalMoves();
        /*
        //  findAllNextMoves();
        printLegalMoves(m_legalMoves);
        //printNextMove(m_nextMove);
        m_io.getMove();
        updateMove();

        while( !verifyLegalMove())
        {
            m_io.getMove();
            updateMove();
        }
        updateChessBoard();
        switchPlayer();
        deleteLegalMoves(m_legalMoves);
        gameLoop();
        */
    }

    //print chessGame for testing
    void printChessGame()
    {
        for(int i = 0; i < m_chessGame.size(); ++i)
            System.out.println( m_chessGame.get(i) );
    }

    private void updateMove()
    {
        m_fromCol = m_io.m_colF;
        m_fromRow = m_io.m_rowF;
        m_toCol = m_io.m_colT;
        m_toRow = m_io.m_rowT;
    }

    //
    private void findPawnMoves(Piece p)
    {
        char col = p.getCol();
        int row = p.getRow();
        //Map<String, Vector<Integer> > innerMap;
        Vector<Integer> vec;
        char colPlusOne = col;
        colPlusOne += 1;
        char colMinusOne = col;
        colMinusOne -= 1;
        int rowPlusOne = row;
        rowPlusOne += 1;
        int rowPlusTwo = row;
        rowPlusTwo += 2;//init move space for 2 vertical
        int rowMinusOne = row;
        rowMinusOne -= 1;
        int rowMinusTwo = row;
        rowMinusTwo -= 2;//init vertical  move for player 2
        int initRowP1 = 2;//initial pawn row for player 1
        int initRowP2 = 7; //initial pawn row for player 2
        Map<Integer, Piece> innerMap;
        Piece pNext;
        Piece path1;
        Piece path2;

        //player 1
        if(p.m_player == PLAYER1)
        {
            //initial move: vertical two spaces
            if( row == initRowP1 )
            {
                innerMap = m_chessBoard.get(col);
                path1 = innerMap.get(rowPlusOne);
                path2 = innerMap.get(rowPlusTwo);

                //verify both forward squares are empty
                if(path1.m_pieceType == EMPTY && path2.m_pieceType == EMPTY)
                {
                    addMoveToLegalMoves(p, col, rowPlusTwo);
                    addMoveToNextMove(p, col, rowPlusTwo);
                }
            }
            //verify single vertical move
            if(rowPlusOne <= MAX_ROW )
            {
                innerMap = m_chessBoard.get(col);
                p = innerMap.get(rowPlusOne);

                //empty row, add move
                if(p.m_pieceType == EMPTY)
                {
                    addMoveToLegalMoves(p, col, rowPlusOne);
                    addMoveToNextMove(p, col, rowPlusOne);
                }
            }

            //diagonal move to take opponent
            if(rowPlusOne <= MAX_ROW && colPlusOne <= MAX_COL)
            {
                //diagonal pawn move to take piece
                //verify +col diag move
                innerMap = m_chessBoard.get(colPlusOne);
                pNext = innerMap.get(rowPlusOne);

                //opponents piece
                if(pNext.m_pieceType != EMPTY && pNext.m_player != m_player)
                {
                    addMoveToLegalMoves(p, colPlusOne, rowPlusOne);
                    addMoveToNextMove(p, colPlusOne, rowPlusOne);
                    //king in check
                    if(pNext.m_pieceType == KING)
                    {
                        //set king as inCheck
                        if(m_player == PLAYER1)
                            m_inCheckP2 = true;
                        else{
                            m_inCheckP1 = true;
                        }
                    }
                }
            }
            //diagonal move
            if(rowPlusOne <= MAX_ROW && colMinusOne >= MIN_COL)
            {
                innerMap = m_chessBoard.get(colMinusOne);
                pNext = innerMap.get(rowPlusOne);

                if(pNext.m_pieceType != EMPTY && pNext.m_player != m_player)
                {
                    addMoveToLegalMoves(p, colMinusOne, rowPlusOne);
                    addMoveToNextMove(p, colMinusOne, rowPlusOne);
                    //king in check
                    if(pNext.m_pieceType == KING)
                    {
                        //set king as inCheck
                        if(m_player == PLAYER1)
                            m_inCheckP2 = true;
                        else{
                            m_inCheckP1 = true;
                        }
                    }
                }
            }
        }
        else
        {
            //PLAYER 2
            if(p.m_player == PLAYER2)
            {
                innerMap = m_chessBoard.get(col);

                //initial move: vertical two spaces
                if(row == initRowP2)
                {
                    path1 = innerMap.get(rowMinusOne);
                    path2 = innerMap.get(rowMinusTwo);

                    //verify empty path
                    if(path1.m_pieceType == EMPTY && p.m_pieceType == EMPTY)
                    {
                        addMoveToLegalMoves(p, col, rowMinusTwo);
                        addMoveToNextMove(p, col, rowMinusTwo);
                    }
                }

                //verify single vertical move
                if(rowMinusOne >= MIN_ROW )
                {
                    pNext = innerMap.get(rowMinusOne);
                    //empty row, add move
                    if(pNext.m_pieceType == EMPTY)
                    {
                        addMoveToLegalMoves(p, col, rowMinusOne);
                        addMoveToNextMove(p, col, rowMinusOne);
                    }
                }

                //diagonal move to take opponent piece
                if(rowMinusOne >= MIN_ROW && colPlusOne <= MAX_COL)
                {
                    //verify +col diag move
                    innerMap = m_chessBoard.get(colPlusOne);
                    pNext = innerMap.get(rowMinusOne);
                    if(pNext.m_pieceType != EMPTY && pNext.m_player != m_player)
                    {
                        addMoveToLegalMoves(p, colPlusOne, rowMinusOne);
                        addMoveToNextMove(p, colPlusOne, rowMinusOne);
                        //king in check
                        if(pNext.m_pieceType == KING)
                        {
                            //set king as inCheck
                            if(m_player == PLAYER1)
                                m_inCheckP2 = true;
                            else{
                                m_inCheckP1 = true;
                            }
                        }
                    }
                }
                //diagonal move
                if(rowMinusOne >= MIN_ROW && colMinusOne >= MIN_COL)
                {
                    //verify -col diag move
                    innerMap = m_chessBoard.get(colMinusOne);
                    pNext = innerMap.get(rowMinusOne);

                    if(pNext.m_pieceType != EMPTY && pNext.m_player != m_player)
                    {
                        addMoveToLegalMoves(p, colMinusOne, rowMinusOne);
                        addMoveToNextMove(p, colMinusOne, rowMinusOne);
                        //king in check
                        if(pNext.m_pieceType == KING)
                        {
                            //set king as inCheck
                            if(m_player == PLAYER1)
                                m_inCheckP2 = true;
                            else{
                                m_inCheckP1 = true;
                            }
                        }
                    }
                }
            }
        }
    }



    void initRenderer( AndroidRenderer renderer)
    {
        m_renderer = renderer;
    }


    void initIO( AndroidIO io)
    {
        m_io = io;
    }
    void initPiece( Piece p)
    {
        m_piece = p;
    }

    //print next move map for testing only
    void printNextMove(Map< Piece, Map<String, Vector<Integer> > > nextMove)
    {
/*
NEXT_MOVE::const_iterator movesIter = nextMove.begin();
for( ; movesIter != nextMove.end(); ++movesIter) // iter over pieces
{
    cout << "COL: [" << movesIter->first->getCol() << "]" << " "
         << "ROW: [" << movesIter->first->getRow() << "]" << endl;
    cout << "TYPE: [" << movesIter->first->m_pieceType << "]" << endl;
    std::map<char, std::vector<int> > innerMap = movesIter->second;
    std::map<char, std::vector<int> >::iterator innerMapIter = innerMap.begin();
    for( ; innerMapIter != innerMap.end(); ++innerMapIter )
    {
        char col = innerMapIter->first;
        std::vector<int> row = innerMapIter->second;
        int length = row.size();

        for( int i=0; i< length; ++i)
        {
            cout << "  " << col << row[i];
        }
        cout << endl;
    }
    cout << endl << endl;
}
*/
    }

    //print legal moves map for testing only
    private void printLegalMoves(Map<Piece, Map<String, Vector<Integer>>> legalMoves)
    {
/*
cout << "legal moves" << endl;
LEGAL_MOVES::const_iterator movesIter = legalMoves.begin();
for( ; movesIter != legalMoves.end(); ++movesIter) // iter over pieces
{
    cout << "COL: [" << movesIter->first->getCol() << "]" << " "
         << "ROW: [" << movesIter->first->getRow() << "]" << endl;
    cout << "TYPE: [" << movesIter->first->m_pieceType << "]" << endl;
    std::map<char, std::vector<int> > innerMap = movesIter->second;
    std::map<char, std::vector<int> >::iterator innerMapIter = innerMap.begin();
    for( ; innerMapIter != innerMap.end(); ++innerMapIter )
    {
        char col = innerMapIter->first;
        std::vector<int> row = innerMapIter->second;
        int length = row.size();

        for( int i=0; i< length; ++i)
        {
            cout << "  " << col << row[i];
        }
        cout << endl;
    }
    cout << endl << endl;
}
*/
    }



    private void initEmptyChessBoard()
    {
        //init empty piece chessBoard
        for(char ch = 'a'; ch <= 'h'; ++ch)
        {
            Map<Integer, Piece> innerMap = new HashMap<Integer, Piece>();
            //init empty inner map
            for(int i = 1; i < 9; ++i)
            {
                Piece piece = m_piece.getPiece(ch, i);
                innerMap.put(i, piece);
            }
            String s = Character.toString(ch);
            m_chessBoard.put(s, innerMap);
        }
    }


    private void addPieceToChessBoard(int player, char col, int row, char type)
    {
        String s = Character.toString(col);
        Map<Integer, Piece > innerMap = m_chessBoard.get(s);

        Piece p  = m_chessBoard.get(s).get(row);
        assert p != null;
        //Piece p = innerMap.get(row);
        p.m_pieceType = type;
        p.m_player = player;
        innerMap.put(row, p);

        m_chessBoard.put(s, innerMap);
    }

    void initChessBoard()
    {
        Map<Integer, Piece > innerMap ;
        int p1 = PLAYER1;
        int p2 = PLAYER2;
        char type;
        String s;
        char col;
        int row;

        initEmptyChessBoard();

        type = PAWN;
        //populate pawns
        for(char ch = COL_A; ch <= COL_H; ++ch)
        {
            //player 1
            row = 2;
            addPieceToChessBoard(p1, ch, row, type);

            //player 2
            row = 7;
            addPieceToChessBoard(p2, ch, row, type);
        }

        type = ROOK;
        //Player 2: set rooks
        col = COL_A;
        row = 8;
        addPieceToChessBoard(p2, col, row, type);

        col = COL_H;
        addPieceToChessBoard(p2, col, row, type);

        //Player 1: set rooks
        col = COL_A;
        row = 1;
        addPieceToChessBoard(p1, col, row, type);

        col = COL_H;
        row = 1;
        addPieceToChessBoard(p1, col, row, type);

        //set knights
        type = KNIGHT;
        //player 2
        col = COL_B;
        row = 8;
        addPieceToChessBoard(p2, col, row, type);

        col = COL_G;
        addPieceToChessBoard(p2, col, row, type);

        //player 1
        col = COL_B;
        row = 1;
        addPieceToChessBoard(p1, col, row, type);

        col = COL_G;
        row = 1;
        addPieceToChessBoard(p1, col, row, type);

        //set bishops
        type = BISHOP;
        //player 2
        col = COL_C;
        row = 8;
        addPieceToChessBoard(p2, col, row, type);

        col = COL_F;
        addPieceToChessBoard(p2, col, row, type);
        //player 1
        row = 1;
        col = COL_C;
        addPieceToChessBoard(p1, col, row, type);
        col = COL_F;
        addPieceToChessBoard(p1, col, row, type);

        //set kings
        type = KING;
        //player 2
        col = COL_D;
        row = 8;
        addPieceToChessBoard(p2, col, row, type);
        //player 1
        row = 1;
        addPieceToChessBoard(p1, col, row, type);

        //set queens
        //player 2
        type  = QUEEN;
        col = COL_E;
        row = 8;
        addPieceToChessBoard(p2, col, row, type);
        //player 1
        row = 1;
        addPieceToChessBoard(p1, col, row, type);
        return;
    }


    //
    private void updateChessBoard()
    {
        Map<Integer, Piece> innerMap;
        String s;
        //store my piece data
        innerMap = m_chessBoard.get(m_fromCol);
        Piece piece = innerMap.get(m_fromRow);
        char type = piece.m_pieceType;

        //mark piece has moved from init position
        piece.m_initPosition = false;

        int player = piece.m_player;

        //reset piece data for "from" square
        piece.m_pieceType = EMPTY;
        piece.m_player = NONE;
        s = Character.toString(m_fromCol);
        m_chessBoard.put(s, innerMap);

        //set piece data for "to" square
        innerMap = m_chessBoard.get(m_toCol);
        piece = innerMap.get(m_toRow);

        piece.m_pieceType = type;
        piece.m_player = player;
        s = Character.toString(m_toCol);
        m_chessBoard.put(s, innerMap);
    }

    private void switchPlayer()
    {
        //switch player
        if(m_player == 1)
            m_player = 2;
        else
        {
            m_player = 1;
        }
        System.out.println( "player: "  + m_player);
    }




}
