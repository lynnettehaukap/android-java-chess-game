package com.example.haukap.chess;

import com.example.haukap.chess.Piece;

import java.util.*;
import android.content.Context;
/**
 * Created by haukap on 7/17/17.
 */

public interface ChessRenderer {

    void printChessBoard( Map<String, Map<Integer, Piece>> m) ;
}
