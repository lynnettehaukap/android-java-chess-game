package com.example.haukap.chess;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.Button;
import android.widget.Toast;
import android.view.MotionEvent;


public class MainActivity extends AppCompatActivity {

    ChessEngine engine = new ChessEngine();
    AndroidIO io = new AndroidIO();
    AndroidRenderer renderer = new AndroidRenderer();
    Piece piece = new Piece();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setMsgButton();
        setRedoButton();
        setMenuButton();
        engine.initRenderer(renderer);
        engine.initIO(io);
        engine.initPiece(piece);
        engine.initChessBoard();
        engine.gameLoop();
       // setPieceLocations();
        return;
    }


    @SuppressWarnings("UnusedAssignment")
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        String s = Integer.toString(x);
        char ch = s.charAt(0);


        //engine.m_buttonFromCol = ch;
        //engine.m_buttonFromRow = y;


        return true;
    }


/*


    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.redoButton:
                // do your code
                break;

            case R.id.msgButton:
                setMsgButton();
                break;

            case R.id.menuButton:
                // do your code
                break;

            default:
                break;
        }

    }
    */


    private void setMsgButton() {
        final Button msgButton = (Button) findViewById(R.id.msgButton);

        msgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "stuff happened",
                        Toast.LENGTH_LONG).show();
            }

        });

    }

    private void setRedoButton() {
        final Button redoButton = (Button) findViewById(R.id.redoButton);

        redoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "redo button hit",
                        Toast.LENGTH_LONG).show();
            }

        });

    }

    private void setMenuButton() {
        final Button menuButton = (Button) findViewById(R.id.menuButton);

        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "menu button hit",
                        Toast.LENGTH_LONG).show();
            }

        });

    }
/*
    private void setMsgButton() {
        final Button msgButton = (Button) findViewById(R.id.msgButton);

        msgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "stuff happened",
                        Toast.LENGTH_LONG).show();
            }

        });

    }

    public void changeButtonText(ImageView imageview)
    {
       // imageview = (ImageView)findViewById(R.id.imageViewA);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.diagram);

        imageview.setImageDrawable(drawable);
    }
    */

    private void setPieceLocations()
    {


        int row;
        char col;
        int y;//top, y position
        int x;

        //Col a
        col = 'a';
        ImageButton pieceButton = (ImageButton) findViewById(R.id.a1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        return;

        //engine.addPieceLocation(x, y, col, row);
/*
        pieceButton = (ImageButton) findViewById(R.id.a2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.a3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.a4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.a5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.a6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.a7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.a8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);

        //col b
        col = 'b';
        pieceButton = (ImageButton) findViewById(R.id.b1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.b2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.b3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.b4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.b5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.b6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.b7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.b8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);


        //col c
        col = 'c';
        pieceButton = (ImageButton) findViewById(R.id.c1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.c2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.c3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.c4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.c5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.c6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.c7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.c8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);


        //col d
        col = 'd';
        pieceButton = (ImageButton) findViewById(R.id.d1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.d2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.d3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.d4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.d5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.d6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.d7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.d8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);

        //col e
        col = 'e';
        pieceButton = (ImageButton) findViewById(R.id.e1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.e2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.e3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.e4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.e5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.e6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.e7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.e8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);

        //col f
        col = 'f';
        pieceButton = (ImageButton) findViewById(R.id.f1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.f2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.f3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.f4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.f5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.f6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.f7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.f8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);

        //col g
        col = 'g';
        pieceButton = (ImageButton) findViewById(R.id.g1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.g2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.g3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.g4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.g5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.g6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.g7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.g8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);

        //col h
        col = 'h';
        pieceButton = (ImageButton) findViewById(R.id.h1);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 1;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.h2);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 2;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.h3);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 3;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.h4);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 4;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.h5);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 5;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.h6);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 6;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.h7);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 7;
        engine.addPieceLocation(x, y, col, row);

        pieceButton = (ImageButton) findViewById(R.id.h8);
        y = pieceButton.getTop();//y position
        x = pieceButton.getLeft();
        row = 8;
        engine.addPieceLocation(x, y, col, row);
*/
    }
}