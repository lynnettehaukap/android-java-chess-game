package com.example.haukap.chess;

/**
 * Created by haukap on 7/17/17.
 */

public class Piece {

    public boolean m_initPosition;
    public char m_pieceType;
    public char m_col;
    public int m_row;
    public int m_player;
    public int m_pieceId;

    Piece()
    {
        m_initPosition = true;
        m_pieceType = 'O';
        m_col = 'z';
        m_row = 0;
        m_player = 0;
        m_pieceId = 0;
    }

    Piece(char col, int row)
    {
        m_initPosition = true;
        m_pieceType = 'O';
        m_col = col;
        m_row = row;
        m_player = 0;
        m_pieceId = 0;
    }
    public char getCol(){return m_col;}
    public int getRow() {return m_row;}

    public Piece getPiece(char col, int row)
    {
        return new Piece(col, row);
    }

}
